# Changelog

#### 1.0.3

- Chore: support base `3.0`.

#### 1.0.2

- Fix: import all used instances in the DCL of `Data.GenHash`.

#### 1.0.1

- Chore: allow containers ^2.

## 1.0.0

- Initial version, import modules from clean platform v0.3.38 and destill all
  generic hashing modules.
