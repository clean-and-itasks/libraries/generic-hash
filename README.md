# generic-hash

This library provides a generic hashing function, with the basic building
blocks taken from MurmurHash2 (https://github.com/aappleby/smhasher).

This hash is not cryptographically secure, but generates few collisions and
is relatively fast. It can therefore be used to speed up comparisons
(https://softwareengineering.stackexchange.com/a/145633), in particular to
create keys for efficient `Map`s.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.
